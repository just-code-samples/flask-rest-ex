import os
from core.tasks import celery


class CeleryConfig:
    enable_utc = True
    timezone = 'Europe/Moscow'


    broker_url = f"redis://{os.environ.get('CELERY_BROKER_HOST')}:6379/0"
    result_expires = 3600

    imports = (
        'core.tasks.upload_tracks',
    )


celery.config_from_object(CeleryConfig)

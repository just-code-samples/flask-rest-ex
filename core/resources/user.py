from flask import request
from flask_restplus import Resource, fields, Namespace

from core.models import UserModel
from core.schemas import UserSchema


api_user = Namespace('user', description='user related operations')


api_user_model = api_user.model('user', {
    'uid': fields.Integer(min=1, readonly=True, description='user identifier'),
    'email': fields.String(description='user email address'),
    'created_at': fields.DateTime(readonly=True, description='timestamp moment when user was created'),
    'is_stuff': fields.Boolean(readonly=True, description='have user admin privileges or not')
})

@api_user.route('/<int:uid>')
@api_user.param('uid', 'The unique user identifier')
class UserAPI(Resource):

    @api_user.response(200, '{ message: user found, data: {model} }', api_user_model)
    @api_user.response(201, '{ message: user not found }')
    def get(self, uid):
        """Get user by uid"""
        item_data = UserModel.find_by_uid(uid)
        if item_data:
            return {'message': 'User found', 'data': UserSchema.from_orm(item_data).dict()}
        return {'message': 'User not found'}, 201

api_user.add_resource(UserAPI, path='api/v1/user')

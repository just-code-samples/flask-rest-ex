import os
from pathlib import Path
from typing import Union
from uuid import uuid4
from flask import request, current_app
from flask_restplus import Resource, fields, Namespace, reqparse
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

from core.models import TrackModel
from core.tasks import call_upload, save_track_to_db


api_tracks = Namespace('track', description='track related operations')


api_upload_track = api_tracks.model('upload', {
    'uid':      fields.Integer(min=1, readonly=True, description='track identifier'),
    'name':     fields.String(description='cleared, secured file name'),
    'original': fields.String(description='original file name'),
    'is_saved': fields.Boolean(readonly=True, description='boolean value about \
                saved file from directory to db or not')
})

@api_tracks.route('/upload')
@api_tracks.param('files', 'In `html` all tracks must be sending with form name "files"')
class TracksUpload(Resource):

    eng_a   = [i for i in 'abcdefghijklmnopqrstuvwxyz']
    rus_a   = [i for i in 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя']

    def _prepare_folder(self) -> str:
        media_folder = current_app.config.MEDIA_FOLDER
        music_folder = current_app.config.MUSIC_FOLDER
        request_folder_name = uuid4().hex
        request_folder = os.path.join(media_folder, music_folder, request_folder_name)
        Path(request_folder).mkdir(parents=True, exist_ok=True)
        return request_folder

    def _clear_filename(self, filename: str) -> str:
        new_filename = []
        lower_filename = filename.lower().strip().replace(' ', '_')
        for i in lower_filename:
            if i in self.eng_a or i in self.rus_a or i.isdigit():
                new_filename.append(i)
            elif i == '.' or i == '_':
                new_filename.append(i)
            else:
                continue
        return ''.join(new_filename)

    def _handle_filename(self, filename: str, file: FileStorage) -> Union[str, None]:
        if filename != '':
            file_extension = os.path.splitext(filename)[1]
            if file_extension in current_app.config.UPLOAD_EXTENSIONS:
                return self._clear_filename(filename)


    @api_tracks.response(200, '{ message: tracks sending, data: \
                        {added_tracks: [array_of_tracks_names]  \
                        not_add_tracks: [array_of_tracks_names]} }')
    def post(self):
        """Upload tracks"""
        request_folder = self._prepare_folder()
        tracks, not_tracks = [], []
        for file in request.files.getlist('files'):
            filename = secure_filename(file.filename)
            new_filename = self._handle_filename(filename, file)
            if new_filename is None:
                not_tracks.append({'name': file.filename,
                                'reason': 'incorrect file name or extension'})
            else:
                if not TrackModel.check_existis_file(new_filename):
                    track = TrackModel(name=new_filename, original=filename)
                    tracks.append(track)
                    save_path = os.path.join(request_folder, new_filename)
                    file.save(save_path)
                else:
                    not_tracks.append({'name': file.filename,
                                    'reason': 'track already exists'})

        if len(tracks) > 0:
            TrackModel.save_files(tracks)
            call_upload(request_folder)

        return {'message': 'tracks sending', 'data':
                {'added_tracks': [{'name': i.name} for i in tracks],
                'not_add_tracks': not_tracks} }


api_tracks.add_resource(TracksUpload, path='api/v1/tracks')

from flask import request
from flask_restplus import Resource, fields, Namespace
from pydantic import ValidationError

from core.models import UserModel
from core.schemas import RegisterUserSchema, LoginUserSchema, UserAuthSchema


api_auth = Namespace('auth', description='register a new user')


api_auth_signup = api_auth.model('signup', {
        'email': fields.String(required=True, description='user email address'),
        'password': fields.String(required=True, description='user password'),
        'confrim_password': fields.String(required=True, description='confrim user password'),
    })

@api_auth.route('/signup')
class RegisterUser(Resource):

    @api_auth.response(200, '{ message: user successfully created, data: {uid: int} }')
    @api_auth.response(201, '{ message: email already exists }')
    @api_auth.response(400, '{ message: invalid data, data: [error_messages] }')
    @api_auth.expect(api_auth_signup)
    def post(self):
        """Register a new User """
        item_json = request.get_json()
        try:
            item_data = RegisterUserSchema(**item_json)
        except ValidationError as e:
            error_messages = [i.get('msg') for i in e.errors()]
            return {'message': 'invalid data', 'errors': error_messages}, 400

        is_email_exists = UserModel.check_existing_email(item_data.email)
        if is_email_exists:
            return {'message': 'email already exists'}, 201
        else:
            item_model_schema = UserAuthSchema(email=item_data.email, password=item_data.password)
            item_model = UserModel(**item_model_schema.dict())
            item_model.save_to_db()
            return {'message': 'user registered', 'data': {'uid': item_model.uid} }



api_auth_signin = api_auth.model('signin', {
        'email': fields.String(required=True, description='user email address'),
        'password': fields.String(required=True, description='user password'),
    })

@api_auth.route('/signin')
class LoginUser(Resource):

    @api_auth.response(200, '{ message: user successfully authenteticate, data: {uid: int} }')
    @api_auth.response(201, '{ message: incorrect email or password }')
    @api_auth.response(400, '{ message: authenteticate data not found }')
    @api_auth.expect(api_auth_signin)
    def post(self):
        """Authenteticate a exist User """
        item_json = request.get_json()
        try:
            item_data = LoginUserSchema(**item_json)
        except ValidationError as e:
            error_messages = [i.get('msg') for i in e.errors()]
            return {'message': 'invalid data', 'errors': error_messages}, 400

        item_model = UserModel.find_by_email(item_data.email)
        if item_model is None:
            return {'message': 'authenteticate data not found'}, 400

        is_password_correct = item_model.check_pass(item_data.password)
        if not is_password_correct:
            return {'message': 'incorrect email or password'}, 201
        else:
            return {'message': 'user successfully authenteticate', 'data': {'uid': item_model.uid} }


api_auth.add_resource(RegisterUser, path='api/v1/auth')
api_auth.add_resource(LoginUser,  path='api/v1/auth')

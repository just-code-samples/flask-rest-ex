from flask_restplus import Api
from dynaconf import settings


api = Api(
    doc=settings.RESTPLUS_DOC,
    version=settings.RESTPLUS_VERSION,
    title=settings.RESTPLUS_TITLE,
    description=settings.RESTPLUS_DESCRIPTION,
    endpoint='api',
    prefix='/api/v1'
    )


# authentetication systems
from .auth import api_auth
api.add_namespace(api_auth)

# user model api
from .user import api_user
api.add_namespace(api_user)

# tracks model api
from .track import api_tracks
api.add_namespace(api_tracks)

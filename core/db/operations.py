from alembic import config, script, command
from alembic.runtime import migration
import sqlalchemy_utils

from . import db, basedir


def create() -> bool:
    """Create clear database if database not exists"""
    if not sqlalchemy_utils.database_exists(db.engine.url):
        sqlalchemy_utils.create_database(db.engine.url)
    return True


def drop() -> bool:
    """Drop current database if exists"""
    if sqlalchemy_utils.database_exists(db.engine.url):
        sqlalchemy_utils.drop_database(db.engine.url)
    return True


def check() -> bool:
    """Check last alembic `revision` and current database state"""
    ini_file = basedir / 'alembic.ini'
    alembic_config = config.Config(ini_file, ini_section='alembic')
    location = alembic_config.get_section_option('alembic', 'script_location')
    alembic_config.set_section_option('alembic', 'script_location', str(basedir / location))
    migration_script = script.ScriptDirectory.from_config(alembic_config)

    with db.engine.begin() as conn:
        context = migration.MigrationContext.configure(conn)
        if context.get_current_revision() != migration_script.get_current_head():
            return False
        else:
            return True


def upgrade() -> bool:
    """Upgrade database to last alembic `revision`"""
    ini_file = basedir / 'alembic.ini'
    alembic_config = config.Config(ini_file, ini_section='alembic')
    location = alembic_config.get_section_option('alembic', 'script_location')
    alembic_config.set_section_option('alembic', 'script_location', str(basedir / location))

    with db.engine.begin() as conn:
        alembic_config.attributes['connection'] = conn
        command.upgrade(alembic_config, 'head')
        return True


def downgrade() -> bool:
    """Downgrade database to base alembic `revision`"""
    ini_file = basedir / 'alembic.ini'
    alembic_config = config.Config(ini_file, ini_section='alembic')
    location = alembic_config.get_section_option('alembic', 'script_location')
    alembic_config.set_section_option('alembic', 'script_location', str(basedir / location))

    with db.engine.begin() as conn:
        alembic_config.attributes['connection'] = conn
        command.downgrade(alembic_config, 'base')
        return True


def make() -> bool:
    """Make new alembic `revision`"""
    ini_file = basedir / 'alembic.ini'
    alembic_config = config.Config(ini_file, ini_section='alembic')
    location = alembic_config.get_section_option('alembic', 'script_location')
    alembic_config.set_section_option('alembic', 'script_location', str(basedir / location))

    with db.engine.begin() as conn:
        alembic_config.attributes['connection'] = conn
        command.revision(alembic_config, 'current')
        return True

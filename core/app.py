import os
import pathlib
from werkzeug.middleware.proxy_fix import ProxyFix
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api
from flask_cors import CORS

from flask import Flask, Blueprint
from dynaconf import FlaskDynaconf

from core import basedir
from core.db import db
from core.resources import api


def prepare_folders(app: Flask) -> None:
    """Do operation with folders needed for app"""
    media = os.path.join(basedir, app.config.MEDIA_FOLDER)
    music = os.path.join(media, app.config.MUSIC_FOLDER)

    if not pathlib.Path(media).is_dir():
        pathlib.Path(media).mkdir(parents=True, exist_ok=True)

    if not pathlib.Path(music).is_dir():
        pathlib.Path(music).mkdir(parents=True, exist_ok=True)

    tracks = os.path.join(basedir, music)
    folders = [str(p) for p in pathlib.Path(tracks).iterdir() if p.is_dir()]
    for folder in folders:
        try:
            os.rmdir(folder)
        except OSError as e:    # Not remove if directory not empty
            pass



def create_app(config: FlaskDynaconf, db: SQLAlchemy, api: Api) -> Flask:
    """Flask app factory"""

    app: Flask = Flask(__name__)

    config.init_app(app)

    db.init_app(app)

    api.init_app(app)
    app.url_map.strict_slashes = False

    prepare_folders(app)

    return app



config: FlaskDynaconf = FlaskDynaconf()
flask_app: Flask = create_app(config, db, api)
flask_app.wsgi_app = ProxyFix(flask_app.wsgi_app)
CORS(flask_app)

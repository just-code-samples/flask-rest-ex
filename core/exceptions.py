
class AlembicRevisionCreatedExecption(Exception):
    def __init__(self, *args: object, code: str = 'ALEMBIC REVISION CREATE, YOU MUST CHECK IT') -> None:
        super().__init__(*args)
        self.code = code


class DatabaseNotExistsException(Exception):
    def __init__(self, *args: object, code: str = 'DATABASE NOT EXIST') -> None:
        super().__init__(*args)
        self.code = code


class DatabaseNotUpgradeException(Exception):
    def __init__(self, *args: object, code: str = 'DATABASE NOT UPGRADED') -> None:
        super().__init__(*args)
        self.code = code

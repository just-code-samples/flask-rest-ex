from datetime import datetime
from pydantic import BaseModel, validator, EmailStr


class UserAuthSchema(BaseModel):
    email:      str
    password:   str
    created_at: datetime = None #datetime.utcnow()
    is_stuff    = False

    @validator('created_at', pre=True, always=True)
    def set_created_at(cls, v):
        return v or datetime.utcnow()


class RegisterUserSchema(BaseModel):
    email:              EmailStr
    password:           str
    confrim_password:   str

    @validator('password')
    def password_must_contain(cls, v):
        if len(v) < 6:      # more than 6 characters
            raise ValueError('password must be at least 6 characters')
        if len(v) >= 14:    # lower than 14 characters
            raise ValueError('password must be less than 14 characters')

        digits = [i.isdigit() for i in v]
        lowers = [i.islower() for i in v if not i.isdigit()]

        if True not in digits:  # no one digit
            raise ValueError('password must contain at least one digit')

        if False not in digits: # only digits
            raise ValueError('password cannot contain only digits')

        if False not in lowers: # no one upper character
            raise ValueError('password must contain at least one large character')

        if True not in lowers:  # only upper characters
            raise ValueError('password cannot contain only large characters')

        return v

    @validator('confrim_password')
    def passwords_match(cls, v, values, **kwargs):
        if 'password' in values and v != values['password']:
            raise ValueError('passwords do not match')
        return v


class LoginUserSchema(BaseModel):
    email:      str
    password:   str

from datetime import datetime
from pydantic import BaseModel, ValidationError, validator, EmailStr


class UserSchema(BaseModel):
    email:      str
    created_at: datetime
    is_stuff:   bool

    @validator('created_at', always=True)
    def convert_to_timestamp(cls, v):
        return v.timestamp()

    class Config:
        orm_mode = True

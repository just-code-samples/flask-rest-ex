from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from loguru import logger
from core.db.operations import (
    check,
    create,
    drop,
    upgrade,
    downgrade,
    make,
)


def command_upgrage(app: Flask, db: SQLAlchemy) -> None:
    """Commnad that upgraded database"""
    db.init_app(app)
    with app.app_context():
        create()
        if not check():
            logger.info('Current database state not valid `revision`')
            upgrade()
            logger.info('Database upgraded')
        else:
            logger.info('Database state valid `revision`')


def command_drop(app: Flask, db: SQLAlchemy) -> None:
    """Commnad that dropped database"""
    db.init_app(app)
    with app.app_context():
        drop()
    logger.info('Database dropped')


def command_create(app: Flask, db: SQLAlchemy) -> None:
    """Commnad that created database"""
    db.init_app(app)
    with app.app_context():
        create()
    logger.info('Database created')


def command_downgrade(app: Flask, db: SQLAlchemy) -> None:
    """Commnad that downgraded database"""
    db.init_app(app)
    with app.app_context():
        downgrade()
        logger.info('Database downgraded')


def command_make(app: Flask, db: SQLAlchemy) -> None:
    """Commnad that create new alembic revision file"""
    db.init_app(app)
    with app.app_context():
        make()
        logger.info('New revision checked')

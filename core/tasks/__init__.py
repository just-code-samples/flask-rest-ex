from flask import Flask
from celery import Celery


celery = Celery()


from .upload_tracks import call_upload, save_track_to_db

import os
import pathlib

from core import basedir
from . import celery


@celery.task()
def save_track_to_db(file_path: str) -> None:
    """Background task for uploading file to db"""
    import os
    from core.models import TrackModel
    from core.app import flask_app as app

    file_name = file_path.split('/').pop()
    with app.app_context():
        is_needed, item = TrackModel.check_existis_and_not_saved(file_name)
        if is_needed:
            with open(file_path, 'rb') as f:
                item.file = f.read()
                item.is_saved = True
                item.save_to_db()

        os.remove(file_path)


def call_upload(nested_folder: str) -> None:
    """Wrapper around `save_track_to_db`"""
    track_folder = os.path.join(basedir, nested_folder)
    loaded_files = [str(p) for p in pathlib.Path(track_folder).iterdir() if p.is_file()]
    for i in loaded_files:
        save_track_to_db.apply_async(args=[i], countdown=30)

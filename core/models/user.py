from typing import Type, Union
from datetime import datetime
import bcrypt
from sqlalchemy.sql import func, expression

from core.db import db


class UserModel(db.Model):
    """
        Classic simple `user` model

        `uid`:          unique id of record
        `email`:        email
        `pass_hash`:    hash of user password
        `created_at`:   date and time of created moment
        `is_stuff`:     boolean value about user admin status
    """

    __tablename__ = "user"

    uid         = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    email       = db.Column(db.String, index=True, unique=True, nullable=False)
    pass_hash   = db.Column(db.String, nullable=False)
    created_at  = db.Column(db.DateTime, default=datetime.utcnow,
                    server_default=func.now(), nullable=False)
    is_stuff    = db.Column(db.Boolean, default=False,
                    server_default=expression.false(), nullable=False)

    @property
    def password(self) -> None:
        """Non real field `password`, impliment for setter"""
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password: str) -> None:
        """Non real field `password`, via setter call method `set_pass`"""
        self.set_pass(password)

    def set_pass(self, password: str) -> None:
        """Convert `password` to hash and write self"""
        password = password.encode("utf-8")
        self.pass_hash = str(bcrypt.hashpw(password, bcrypt.gensalt()), "utf8")

    def check_pass(self, password: str) -> bool:
        """Check `password` hash equal a pass_hash from db"""
        if isinstance(password, str):
            password = password.encode("utf-8")
            return bcrypt.checkpw(password, self.pass_hash.encode("utf-8"))
        else:
            return False

    def save_to_db(self) -> None:
        """Save instance to database"""
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_uid(cls, uid: int) -> Union[Type['UserModel'], None]:
        return cls.query.filter_by(uid=uid).first()

    @classmethod
    def find_by_email(cls, email: str) -> Union[Type['UserModel'], None]:
        return cls.query.filter_by(email=email).first()

    @classmethod
    def check_existing_email(cls, email: str) -> bool:
        data = cls.query.filter_by(email=email).first()
        if data is None:
            return False
        else:
            return True

from typing import Type, List
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql import func, expression

from core.db import db


# track_playlist = db.Table('track_playlist', db.metadata,
#     db.Column("uid", db.Integer, primary_key = True),
#     db.Column('track_uid', db.Integer, db.ForeignKey('track.uid')),
#     db.Column('playlist_id', db.Integer, db.ForeignKey('playlist.uid'))
# )


class TrackModel(db.Model):
    """
        Track model for store music

        `uid`:      unique id of record
        `name`:     usable 'cleared', secured file name
        `original`: original file name
        `file`:     binary object stored music file
        `is_saved`: boolean value about saved file from directory to db or not
    """

    __tablename__ = "track"

    uid         = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    name        = db.Column(db.String(128), index=True, unique=True, nullable=False)
    original    = db.Column(db.Text, nullable=True)
    file        = db.Column(db.LargeBinary, nullable=True)
    is_saved    = db.Column(db.Boolean, default=False,
                    server_default=expression.false(), nullable=False)

    def save_to_db(self) -> None:
        """Save instance to database"""
        db.session.add(self)
        db.session.commit()

    @classmethod
    def save_files(cls, tracks: List[Type['TrackModel']]) -> None:
        for track in tracks:
            if isinstance(track, cls):
                db.session.add(track)
        db.session.commit()

    @classmethod
    def check_existis_file(cls, name: str) -> bool:
        data = cls.query.filter_by(name=name).first()
        if data is None:
            return False
        else:
            return True

    @classmethod
    def check_existis_and_not_saved(cls, name: str) -> bool:
        data = cls.query.filter_by(name=name).first()
        if data is not None:        # if file exists
            if not data.is_saved:   # and file not saved
                return True, data
        return False, None

    # playlists = relationship(
    #     "PlaylistModel",
    #     secondary=track_playlist_association,
    #     primaryjoin=(track_playlist.c.track_uid == uid),
    #     secondaryjoin=(track_playlist.c.playlist_uid == uid),
    #     backref=db.backref(
    #         'track_playlist',
    #         lazy='dynamic'
    #     ),
    #     lazy='dynamic'
    # )

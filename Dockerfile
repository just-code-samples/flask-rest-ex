FROM python:3.8.5

ENV TZ=Asia/Yekaterinburg
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN pip install --upgrade pip

WORKDIR /core
COPY ./requirements.txt /core/
RUN pip install -r requirements.txt

COPY ./cli.py ./config.py ./settings.toml ./alembic.ini /core/
COPY ./core /core/core

ENV FLASK_ENV=production
ENTRYPOINT ["python", "cli.py"]

EXPOSE 8080/tcp

# from __future__ import absolute_import, unicode_literals

import click
from flask import Flask
from loguru import logger
from gunicorn.app.base import BaseApplication

from core.commands import (
    command_upgrage,
    command_drop,
    command_create,
)
from core.exceptions import (
    DatabaseNotExistsException,
    DatabaseNotUpgradeException,
)
from core.db import db
from core.app import flask_app as app
from core.celeryconfig import celery


class TwoModeCliException(Exception):
    def __init__(self, *args: object, code: str = "CAN'T RUN IN dev AND production \
        MODE AT THE SAME TIME") -> None:
        super().__init__(*args)
        self.code = code


@click.command()
@click.option("--drop", is_flag=True, help="Drop database")
@click.option("--create", is_flag=True, help="Create database")
@click.option("--upgrade", is_flag=True, help="Upgrade database")
@click.option("--dev", is_flag=True, help="Run app in dev mode")
@click.option("--production", is_flag=True, help="Run app in production mode")
@click.option("--celery", is_flag=True, help="Run celery workers")
def cli(drop, create, upgrade, dev, production, celery):
    """click commnads entrypoint"""

    if dev and production:
        raise TwoModeCliException()

    if drop:
        command_drop(app, db)
        if not create:
            raise DatabaseNotExistsException()

    if create:
        command_create(app, db)
        if not upgrade:
            raise DatabaseNotUpgradeException()

    if upgrade:
        command_upgrage(app, db)

    if dev:
        run_dev(app)

    if production:
        run_production(app)

    if celery:
        run_celery()



def run_dev(app: Flask) -> None:
    """Run Flask app"""
    app.config.setenv('development')
    app.run(
        host=app.config.host,
        port=app.config.port,
        threaded=app.config.threaded
    )


class ProductionApplication(BaseApplication):
    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super().__init__()

    def load_config(self):
        config = {
            key: value
            for key, value in self.options.items()
            if key in self.cfg.settings and value is not None
        }
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


def run_production(app: Flask) -> None:
    """"Running gunicorn programally"""
    app.config.setenv('production')
    options = {
        "bind": "%s:%s" % ("0.0.0.0", "8080"),
        "workers": 1,
    }
    ProductionApplication(app, options).run()


def run_celery() -> None:
    argv = [
        'worker',
        '--loglevel=INFO',
    ]
    celery.worker_main(argv)


if __name__ == '__main__':
    try:
        cli()
    except DatabaseNotExistsException:
        logger.info("App can't work without database")
    except DatabaseNotUpgradeException:
        logger.info("App can't work if database not upgraded")
    except TwoModeCliException:
        logger.info("can't run in dev and production mode at the same time")

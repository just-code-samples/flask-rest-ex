# core


### Commands

Get requirements without pipenv dependencies:
    `pipenv lock --keep-outdated --requirements > requirements.txt`

Run Celery:  
  `celery -A core.app.celery worker --loglevel=info`

### Docker commands

 - Build Docker image:  
  `docker build -t core .`

- Run Docker container:
  `docker run --env-file ./.env -e FLASK_ENV='production' --name gfp-core -p 8080:8080 --network="host" core`

- Run Docker dev:  
  `docker-compose -f docker-compose.dev.yaml -p gf-dev up`


### Gitlab as docker repository:

  `docker login registry.gitlab.com`  

  `docker build -t registry.gitlab.com/gf-proj/core .`  

  `docker push registry.gitlab.com/gf-proj/core`  



### Gitlab as docker repository:

  `docker login registry.gitlab.com`  

  `docker build -t registry.gitlab.com/gf-proj/core .`  

  `docker push registry.gitlab.com/gf-proj/core`  


### Endpoints

1. Swagger docs can find in url:
  `host:port/docs`

2. If .dev:
  - pgadmin: 127.0.0.1:5050
  - redis-commander: 127.0.0.1:8090

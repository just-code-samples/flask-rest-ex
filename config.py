
from dynaconf import Dynaconf

settings = Dynaconf(
    envvar_prefix="FLASK",
    settings_files=['settings.toml', '.secrets.toml',],
    load_dotenv=True
)

# `envvar_prefix` = export envvars with `export DYNACONF_FOO=bar`.
# `settings_files` = Load this files in the order.
